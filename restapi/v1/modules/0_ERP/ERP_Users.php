<?php
/**
 *
 * @About:      Clase con las funciones de usuario
 * @File:       ERP_Users.php
 * @Date:       $Date:$ Abr-2018
 * @Version:    $Rev:$ 1.0
 * @Developer:  Xavier Vilella (vilellamunoz@gmail.com)
 **/
class ERP_Users {
 
    private $db;
 
    function __construct() {
        $this->db = new DbHandler();
    }
 
    
    /**
     * Devuelve todos los usuarios que existen
     * @return array usuarios
     */
    function getAllUsers(){
    
        $usuarios = $this->db->query("select * from 0_ERP_USUARIOS");
    
        return $usuarios;
    
    }
    
    
    /**
     * Devuelve un usuario buscado por su id
     * @param int $id
     * @return array usuario
     */
    function getUserById($id){
    
        $usuarios = $this->db->query("select * from 0_ERP_USUARIOS where id_usuario = $id");
    
        return $usuarios;
    
    }
    
    /**
     * Devuelve un usuario buscado por su api-key
     * @param string $api_key
     * @return array usuario
     */
    function getUserByApiKey($api_key){
    
        $usuarios = $this->db->query("select * from 0_ERP_USUARIOS where api_key = '$api_key'");
    
        return $usuarios;
    
    }
    
    /**
     * Devuelve un usuario buscado por su email y password
     * @param string $email
     * @param string $password
     * @return array usuario
     */
    function getUserByEmailAndPassword($email,$password){
    
        $password =  encrypt($password);
        
        $usuario = $this->db->query("select * from 0_ERP_USUARIOS where email = '$email' AND password = '$password'");
    
        return $usuario;
    
    }
    
    
    /**
     * Crea un nuevo usuario
     * 
     * @param string $email
     * @param string $password
     * @return devuelve el usuario si todo ok, o el mensaje de error si no se ha podido crear
     */
    function insertUser( $email, $password){
        
        validateEmail($email);
        
        validatePassword($password);
        
        $api_key = encrypt($email.$password);
        
        $password = encrypt($password);
        
        $resultado = $this->db->insert("INSERT INTO 0_ERP_USUARIOS 
                                            (email,password,api_key,created_at) 
                                        VALUES ('$email','$password','$api_key', NOW())");
        
        if($resultado['error']){
            return $resultado['message'];
        }
        else{
            return $this->getUserById($resultado['id']);
        }
        
    }
    
    
    
    /**
     * Valida si un usuario tiene permiso o no para utilizar esa funcion
     * @param string $metodo
     * @param string $accion
     * @return boolean
     */
    function tienePermiso($metodo,$accion,$tabla){
        
        $id_rol = $_SESSION['usr']->id_usuario;
        
        $permiso = $this->db->query("select * from 0_ERP_ROLES_ACCIONES where
                                           metodo = '$metodo' AND url = '$accion' AND id_rol =  (select rol from 1_EMPRESAS_USUARIOS where id_usuario = 4)  AND active = 1");
        
        if(!isset($permiso[0])){
            return false;
        }
        
        return true;
        
    }
    
    
    
 
}