<?php
/**
 *
 * @About:      Clase con las funciones de usuario
 * @File:       ERP_Users.php
 * @Date:       $Date:$ Abr-2018
 * @Version:    $Rev:$ 1.0
 * @Developer:  Xavier Vilella (vilellamunoz@gmail.com)
 **/
class Empresas {
 
    private $db;
 
    function __construct() {
        $this->db = new DbHandler();
    }
 
    /**
     * Crea una nueva empresa
     * 
     * @param string $empresa
     * @return devuelve la empresa si todo ok, o el mensaje de error si no se ha podido crear
     */
    function insertEmpresa($empresa){
        
        
        $resultado = $this->db->insert("INSERT INTO 1_EMPRESAS 
                                            (nombre,created_at) 
                                        VALUES ('$empresa', NOW())");
        
        if($resultado['error']){
            return $resultado['message'];
        }
        else{
            return $this->getEmpresaById($resultado['id']);
        }
        
    }
    
    /**
     * Devuelve una empresa buscada por su id
     * @param int $id
     * @return array empresa
     */
    function getEmpresaById($id){
    
        $empresa = $this->db->query("select * from 1_EMPRESAS where id_empresa = $id");
    
        return $empresa;
    
    }
    
    /**
     * Devuelve los usuarios que tiene una empresa
     * @param type $id
     * @return type
     */
    function getUsuariosEmpresa($id){
        
        $usuarios = $this->db->query("select * from 1_EMPRESAS_USUARIOS where id_empresa = $id");
        
        return $usuarios;
    }
    
    /**
     * Añade un usuarios a una empresa
     * 
     * @param int $empresa
     * @param int $usuario
     * @return devuelve los usuarios de la empresa si todo ok, o el mensaje de error si no se ha podido crear
     */
    function insertUsuariosEmpresa($empresa,$usuario,$rol){
        
        
        $resultado = $this->db->insert("INSERT INTO 1_EMPRESAS_USUARIOS 
                                            (id_empresa,id_usuario,rol) 
                                        VALUES ('$empresa', '$usuario','$rol')");
        
        if($resultado['error']){
            return $resultado['message'];
        }
        else{
            return $this->getUsuariosEmpresa($empresa);
        }
        
    }
    
    
    
 
}