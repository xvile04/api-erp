<?php


/*********************** USEFULL FUNCTIONS **************************************/

/**
 * Verificando los parametros requeridos en el metodo o endpoint
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
 
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(400, $response);
        
        $app->stop();
    }
}
 
/**
 * Validando parametro email si necesario; un Extra ;)
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * valida si el nombre de usuario tiene la longitud y formato correcto
 * @param String $string
 */
function validateUsername($string){
    $app = \Slim\Slim::getInstance();
    if(strlen($string) > 20 || preg_match('/[^A-Za-z0-9.\\-]/', $string)){
        $response["error"] = true;
        $response["message"] = 'Usename is not valid, max lenght 20 characters and contain valid caracters: a-z, 0-9, - , .';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * valida si el password tiene la longitud y formato correcto
 * @param String $string
 */
function validatePassword($string){
    $app = \Slim\Slim::getInstance();
    if(strlen($string) > 20 || preg_match('/[^A-Za-z0-9.\\-]/', $string)){
        $response["error"] = true;
        $response["message"] = 'Password is not valid, max lenght 20 characters and contain valid caracters: a-z, 0-9, - , .';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Convierte una respuesta de array de la base de datos a json y lo imprime
 * @param Array $results resultados
 * @param String $message mensaje que queremos enviar
 * @param Int $error codigo de error
 */
function dbResponseToEchoResponse($results,$message = '', $error = false){
    $response = array();
    
    $response["error"] = $error;
    $response["num_results"] = count($results);
    $response["message"] = $message; 
    $response["results"] = $results;

    echoResponse(200, $response);
}
 
/**
 * Mostrando la respuesta en formato json al cliente o navegador
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
 
    // setting response content type to json
    $app->contentType('application/json');
 
    echo json_encode($response);
    
}

/**
 * Agregando un leyer intermedio e autenticación para uno o todos los metodos, usar segun necesidad
 * Revisa si la consulta contiene un Header "Authorization" para validar
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
 
    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        
        // get the api key
        $api_key = $headers['Authorization'];
        
        $ERP_Users = new ERP_Users();
        $user = $ERP_Users->getUserByApiKey($api_key);
        
        // validating api key hacer que valide con la base de datos la key de cada usuario
        if (!isset($user[0])) { 
            
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Acceso denegado. Token inválido";
            echoResponse(401, $response);
            
            $app->stop(); //Detenemos la ejecución del programa al no validar
            
        } 
        
        
        // guardamos el usuario de en la sesion
        $_SESSION['usr'] = $user[0];
        
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Falta token de autorización";
        echoResponse(400, $response);
        
        $app->stop();
    }
}

/**
 * Función que resvisa si un usuario tiene los permisos necesarios para realizar
 * la accion
 */
function validatePermisions($metodo,$accion,$tabla) {
    
    $app = \Slim\Slim::getInstance();
    
    $ERP_Users = new ERP_Users();
    if(!$ERP_Users->tienePermiso($metodo,$accion,$tabla)){
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "No tienes permisos para esta acción";
        echoResponse(400, $response);
        
        $app->stop();
    }
    
}


/**
 * Encripta un string para guardarlo a la base de datos
 * 
 * @param String $string
 * @param String $key
 * @return String
 */
function encrypt($string, $key = 'DxB0NcP3YBWgfsN1CB1L'){
    $result = '';
    for($i=0; $i<strlen($string); $i++){
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) -1 ,1);
        $char = chr(ord($char)+ord($keychar));
        $result .= $char;
    }
    return base64_encode($result);
}

/**
 * 
 * desencripta un string
 * 
 * @param String $string
 * @param String $key
 * @return String
 */
function decrypt($string, $key = 'DxB0NcP3YBWgfsN1CB1L'){
    $result = '';
    $string = base64_decode($string);
    for($i=0; $i<strlen($string); $i++){
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) -1 ,1);
        $char = chr(ord($char)-ord($keychar));
        $result .= $char;
    }
    return $result;
}