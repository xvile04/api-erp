<?php


/**
 * [GET] /usuario/idUsuario --> devuelve un usuario en concreto
 */
$app->get('/usuario/:idUsuario', 'authenticate', function($idUsuario) {
    
    validatePermisions('GET', '/usuario/:idUsuario');
    
    $ERP_Users = new ERP_Users();
    
    $usuarios = $ERP_Users->getUserById($idUsuario);
    
    dbResponseToEchoResponse($usuarios);
});


/**
 * [GET] /usuario --> devuelve todos los usuarios que tenemos
 */
$app->get('/usuario', 'authenticate', function() {
    
    validatePermisions('GET', '/usuario');
    
    $ERP_Users = new ERP_Users();
    
    $usuarios = $ERP_Users->getAllUsers();
    
    dbResponseToEchoResponse($usuarios);
});


/**
 *  [POST] /usuario --> crea un nuevo usuario
 */
$app->post('/usuario',  function() use ($app) {
    
    $ERP_Users = new ERP_Users();
    
    // check for required params
    verifyRequiredParams(array( 'email', 'password'));
    
    $email = $app->request->post('email');
    $password = $app->request->post('password');
    
    $usuario = $ERP_Users->insertUser($email,$password);
    
    dbResponseToEchoResponse($usuario);
   
});

/**
 *  [POST] /usuario/login --> loguea un usuario
 */
$app->post('/usuario/login', function() use ($app) {
    
    $ERP_Users = new ERP_Users();
    
    // check for required params
    verifyRequiredParams(array('email', 'password'));
    
    $email = $app->request->post('email');
    $password = $app->request->post('password');
    
    $usuario = $ERP_Users->getUserByEmailAndPassword($email,$password);
    
    
    if(!isset($usuario[0])){
        // email o contraseña erroneos
        $response["error"] = true;
        $response["message"] = "Email o contraseña erroneos";
        echoResponse(400, $response);
        
        $app->stop();
    }
    
    dbResponseToEchoResponse($usuario);
   
});
