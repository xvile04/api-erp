<?php


/**
 *  [POST] /empresa --> crea una nueva empresa
 */
$app->post('/empresa',  function() use ($app) {
    
    $empresas = new Empresas();
    
    // check for required params
    verifyRequiredParams(array( 'nombre_empresa'));
    
    $nombre_empresa = $app->request->post('nombre_empresa');
    
    $empresa = $empresas->insertEmpresa($nombre_empresa);
    
    dbResponseToEchoResponse($empresa);
   
});


/**
 *  [POST] /empresa/usuario --> añade un usuario a una empresa
 */
$app->post('/empresa/usuario', 'authenticate',  function() use ($app) {
    
    // check for required params
    verifyRequiredParams(array( 'empresa','usuario','rol'));
    
    $empresa = $app->request->post('empresa');
    $usuario = $app->request->post('usuario');
    $rol = $app->request->post('rol');
    
    
    $empresas = new Empresas();
    $usuarios = $empresas->getUsuariosEmpresa($empresa);
    
    if(count($usuarios) > 0){
        validatePermisions('POST', '/empresa/usuario','1_EMPRESAS_USUARIOS');
    }
    
    
    $resultado = $empresas->insertUsuariosEmpresa($empresa, $usuario, $rol);
    
    dbResponseToEchoResponse($resultado);
   
});

