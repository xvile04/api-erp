<?php
ini_set('error_reporting', E_ALL);
/**
 *
 * @About:      API Interface
 * @File:       index.php
 * @Date:       $Date:$ Abr-2018
 * @Version:    $Rev:$ 1.0
 * @Developer:  Xavier Vilella (vilellamunoz@gmail.com)
 **/

/* Los headers permiten acceso desde otro dominio (CORS) a nuestro REST API o desde un cliente remoto via HTTP
 * Removiendo las lineas header() limitamos el acceso a nuestro RESTfull API a el mismo dominio
 * Nótese los métodos permitidos en Access-Control-Allow-Methods. Esto nos permite limitar los métodos de consulta a nuestro RESTfull API
 * Mas información: https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
 **/
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"'); 


/* Añadimos el fichero con las funciones que nos pueden ser útiles */
include_once 'helpers/helpers.php';

/* Este modulo lo necesitamos siempre ya que es el que nos permite autenticar usuarios en la api*/
require 'modules/0_ERP/ERP_Users.php';
require 'modules/1_EMPRESAS/Empresas.php';
        

//include_once '../include/Config.php';

/* Puedes utilizar este file para conectar con base de datos incluido en este demo; 
 * si lo usas debes eliminar el include_once del file Config ya que le mismo está incluido en DBHandler 
 **/
require_once '../include/DbHandler.php'; 

require '../libs/Slim/Slim.php'; 
\Slim\Slim::registerAutoloader(); 

$app = new \Slim\Slim();


/* Rutas de las funciones de usuario */
require 'routes/0_ERP_Users.php';

/* Rutas de las funciones de usuario */
require 'routes/1_EMPRESAS.php';


/* corremos la aplicación */
$app->run();



?>