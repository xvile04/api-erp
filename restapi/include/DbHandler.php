<?php
/**
 *
 * @About:      Database connection manager class
 * @File:       Database.php
 * @Date:       $Date:$ Nov-2015
 * @Version:    $Rev:$ 1.0
 * @Developer:  Federico Guzman (federicoguzman@gmail.com)
 **/
class DbHandler {
 
    private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
 
    public function query($sql){
        try{
            $results = array();
            
            $query = $this->conn->query($sql);
            
            while($row=$query->fetch(PDO::FETCH_OBJ)) {
                /*its getting data in line.And its an object*/
                $results []= $row;
            }
            return $results;
            
        }catch(PDOException  $e ){
            die("Error: ".$e);
        }
    }
    
    public function insert($sql)
    {
        $query = $this->conn->prepare($sql);
            
        try{
            $this->conn->beginTransaction();
            
            $query->execute();
            
            $idInsert = $this->conn->lastInsertId();
                    
            $this->conn->commit();
                
            return ['error' => false, 'id' => $idInsert];
            
            
        }catch(PDOException  $e ){
            
            $this->conn->rollback();
            
            return ['error' => true, 'message' => $query->errorCode()];
        }
    }
 
}
 
?>